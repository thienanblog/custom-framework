<?php

class UserController {
	public function index() {
		$users = User::getUsers(false, 10);
		Json::updateData('users', $users);
		Json::printData();
	}

	public function create() {
		$email = Input::get('email');
		$password = Input::get('password');
		$confirm_password = Input::get('confirm_password');

		if (!$email) {			
			Json::warning('Email không được bỏ trống');
		}

		if ($email && User::getUser($email)) {			
			Json::warning('Email đã trùng, vui lòng chọn Email khác');
		}

		if (!$password || !$confirm_password || $password != $confirm_password) {			
			Json::warning('Mật khẩu không trùng hoặc bỏ trống');
		}

		if (Json::isValid()) {
			$user = User::create([
				'email' => $email,
				'password' => App::encryptPassword($password)
			]);						
			Json::messages("Tạo người dùng $email thành công");
		}
		Json::printData();
	}

	public function update($id) {
		$password = Input::get('password');
		$confirm_password = Input::get('confirm_password');

		$current_user = User::find($id);

		if ($password && $password != $confirm_password) {			
			Json::warning('Mật khẩu không trùng hoặc bỏ trống');
		}

		if (Json::isValid()) {
			if ($password && $confirm_password) {
				$current_user->password = App::encryptPassword($password);
			}			
			$current_user->save();			
			Json::messages("Cập nhật người dùng \"$current_user->email\" thành công");
		}
		Json::printData();
	}

	public function show($id) {
		$user = User::getUser($id);
		if ($user) {
			Json::updateData('user', $user);
		}
		Json::printData();
	}

	public function delete($id) {
		if (Input::get()) {
			$user = User::deleteUser($id);
			if ($user) {
				Json::messages("Xóa người dùng \"$user->email\" thành công");				
			}
		}
		Json::printData();
	}
}