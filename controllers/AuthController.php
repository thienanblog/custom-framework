<?php

class AuthController {
	public function login() {
		$email = Input::get('email');
		$password = Input::get('password');

		if (!$email) {
			Json::warning('Vui lòng nhập Email');
		}

		if (!$password) {
			Json::warning('Vui lòng nhập Password') ;
		}

		if ($email && $password && Json::isValid()) {
			$user = User::getUser($email, true);
			if ($user && App::verifyPassword($password, $user['password'])) {
				if ($user['level'] == 1) {
					unset($user['password']);
					Session::update('user', [
						'id' => $user['id'],
						'email' => $user['email'],
						'level' => $user['level']
					]);
					Json::updateData('user',  [
						'id' => $user['id'],
						'email' => $user['email'],
						'level' => $user['level']
					]);
					Json::update('isLogin', true);
					Json::messages('Đăng nhập thành công');
				} else {
					Json::update('isLogin', false);
					Json::warning('Người dùng không đủ quyền hạn.');
				}
			} else {
				Json::update('isLogin', false);
				Json::warning('Thông tin người dùng không chính xác');
			}
		} else {
			Json::warning('Có lỗi xảy ra');
		}

		Json::printData();
	}

	public function logout() {
		Session::destroy();
		Json::update('isLogin', false);
		Json::printData();
	}

	public function isLogin() {
		Json::update('isLogin', true);
		Json::updateData('users', Session::get('user'));
		if (!User::isLogin()) {
			Json::update('isLogin', false);
			Json::warning('Bạn không được phép truy cập');
		}
		Json::printData();
	}
}