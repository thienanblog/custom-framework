<?php
session_start();
require('cores.php');
define('BASEURL', 'your_url');

// Khởi chạy sự kiện
// Event::disPatcher();

App::start();

App::filter(['isLogin', 'login', 'logout'], function() {
	header('Content-Type: application/json');
	return true;
});

App::filter(['users.*'], function() {
	header('Content-Type: application/json');
	if (!User::isLogin()) {				
		Json::warning('Bạn không được phép truy cập');
		Json::printData();
		return false;
	}
	return true;
});

// Auth Controller
Route::get('isLogin', 'AuthController@isLogin');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');

// User Controller
Route::get('users', 'UserController@index');
Route::post('users/create', 'UserController@create');
Route::get('users/{$id}/show', 'UserController@show');
Route::put('users/{$id}/update', 'UserController@update');
Route::delete('users/{$id}/delete', 'UserController@delete');

App::end();